## Descripition

This is a technical test to evaluate my React & NodeJS skills

### Frontend Tecnologies

* ReactJS
* Material UI
* JS-cookie
* JSS
* Redux
* Axios

### Backend Tecnologies

* cors
* dotenv
* express
* express-session
* google-auth-library
* mongoose
* nodemon

## Preview
### Desktop version
![plot](./preview/LoginDesktopPreview.JPG)
![plot](./preview/NotesDesktopPreview.JPG)

### Mobile version
![plot](./preview/LoginMobilePreview.JPG)
![plot](./preview/NotesMobilePreview.JPG)



## How to test


1. You need to have mongodb service

2. Review & configure the .env files to config enviroment constants


In the *backend & frontend* project directory, you can run:

### 1. `npm install`

The project dependencies start to install, when that finish proceed ->

### 2. `npm start`

Open [http://localhost:3000](http://localhost) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
