import { SET_CURRENT_USER} from "../utils/types";

import {getSessionCookie} from '../utils/auth'

import isEmpty from "is-empty";

const user = getSessionCookie()

const initialState = {
  isAuthenticated: !isEmpty(user),
  user,
};

function authReducer (state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      };
    default:
      return state;
  }
}


export default authReducer