import { useState } from 'react'


import { makeStyles } from '@material-ui/core/styles';
import { Card, CardContent, CardActions, IconButton, Typography, Tooltip, TextField } from '@material-ui/core';

import { red } from '@material-ui/core/colors';

import NoteIcon from '@material-ui/icons/Note';
import CreateIcon from '@material-ui/icons/Create';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';



const useStyles = makeStyles((theme) => ({
    cardHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    fullSize: {
        height: '100%',
        width: '100%'
    },
    pad: {
        margin: 5
    },
    warning: {
        color: red[600]
    }
}));

function Note({ data, onEdit, onDelete }) {
    const classes = useStyles()

    let { content, date } = data || { content: 'Default content', id: '' }
    date =  date?new Date(date):new Date()
    const [editing, setEditing] = useState(false);
    const [deleting, setDeleting] = useState(false);

    const [formValues, setFormValues] = useState({ content })
    const handleFormChange = (e) => {
        setFormValues({ ...formValues, [e.target.id]: e.target.value })
    }


    const onRightButtonClicked = (e) => {
        e.preventDefault()
        if (editing) {

            setEditing(false)
        } else if (deleting) {

            setDeleting(false)
        } else {
            setDeleting(true)
        }
    }

    const onLeftButtonClicked = (e) => {
        e.preventDefault()
        if (editing) {

            const { content } = formValues
            const tmp = { ...data, content }
            onEdit && onEdit(data._id, tmp)
            setEditing(false)
        } else if (deleting) {
            onDelete && onDelete(data._id)
            setDeleting(false)
        } else {
            setEditing(true)
        }
    }

    return (
        <Card className={classes.fullSize} >
            <CardActions className={classes.cardHeader}>
                <div className={classes.pad}>
                    {
                        (deleting || editing) ?
                            <Typography variant='subtitle2'>Are you sure?</Typography>
                            :
                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                <NoteIcon color='primary' style={{ marginRight: 5 }} />
                                <Typography variant='subtitle2' component='span'>{date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear()}</Typography>
                            </div>

                    }
                </div>


                <div>
                    <Tooltip title={(deleting || editing) ? 'Confirmar' : 'Editar'}>
                        <IconButton style={{ margin: '1px' }}

                            aria-label="edit"
                            onClick={onLeftButtonClicked} >
                            {
                                (deleting || editing) ? <DoneIcon /> : <CreateIcon />
                            }
                        </IconButton>
                    </Tooltip>

                    <Tooltip title={(deleting || editing) ? 'Cancelar' : 'Eliminar'}>

                        <IconButton style={{ margin: '1px' }} aria-label="delete"
                            onClick={onRightButtonClicked}>
                            {
                                (deleting || editing) ? <CloseIcon /> : <DeleteForeverIcon className={classes.warning} />
                            }
                        </IconButton>
                    </Tooltip>
                </div>

            </CardActions>

            <CardContent>
                {
                    editing ?
                        <TextField
                            id="content"
                            multiline
                            fullWidth
                            value={formValues.content}
                            onChange={handleFormChange}
                        /> :
                        <Typography variant="body2" component="span" noWrap >
                            {content}
                        </Typography>
                }
            </CardContent>


        </Card>
    );
}

export default Note