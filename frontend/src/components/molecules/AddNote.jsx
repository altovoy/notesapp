import { useState } from 'react'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import { Card, Grid, CardActionArea, Typography, TextField, IconButton } from '@material-ui/core'


import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import NoteAddIcon from '@material-ui/icons/NoteAdd';


const useStyles = makeStyles({
    fullSize: {
        height: '100%',
        width: '100%'
    },
    card: {
        padding: '5%',
 
    }
})

function AddNote({ onAdd }) {
    const classes = useStyles()
    const [formValues, setFormValues] = useState({ content: '' })
    const [editing, setEditing] = useState(false)

    const handleFormChange = (e) => {
        setFormValues({ ...formValues, [e.target.id]: e.target.value })
    }

    const handleAdd = (e) => {
        e.preventDefault()
        onAdd && onAdd({...formValues, date: new Date()})
        setFormValues({...formValues, content: '' })
        setEditing(false)
    }

    return (
        <Card className={clsx(classes.fullSize, classes.card)}>
            {editing ? <Grid className={classes.fullSize} container direction='row' alignItems='center' justify='center'>

                <Grid item sm={8} xs={12} >
                    <TextField id="content"
                        fullWidth
                        multiline
                        onChange={handleFormChange} />
                </Grid>
                <Grid item sm={4} xs={12} container direction='row' alignItems='center' justify='center'>
                    <IconButton aria-label="delete"
                        onClick={handleAdd}>
                        <DoneIcon />
                    </IconButton>
                    <IconButton aria-label="delete"
                        onClick={() => { setEditing(false) }}>
                        <CloseIcon />
                    </IconButton>
                </Grid>
            </Grid> :
                <CardActionArea
                    className={classes.fullSize}
                    onClick={() => setEditing(true)}>
                    <Grid
                        container
                        direction='row'
                        justify='center'
                        alignItems='center'
                    >
                        <NoteAddIcon color='primary' style={{ margin: '10px' }} />
                        <Typography style={{ fontWeight: "bold" }} color='primary'>ADD NOTE</Typography>
                    </Grid>
                </CardActionArea>
            }
        </Card>
    )

}

export default AddNote
