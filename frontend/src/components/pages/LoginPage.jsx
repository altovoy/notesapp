import { connect } from 'react-redux'
import { GoogleLogin } from 'react-google-login'
import { useHistory } from 'react-router-dom'
import clsx from 'clsx'
import { googleAuth } from '../../utils/auth'

import {ReactComponent as BitBangIcon} from '../../images/bit-bang-logo.svg'

import {Paper, Icon} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'

const useStyles = makeStyles({
    root: {
        height: '80vh',
        margin: '3%',
    },
    dispCenter: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    paper: {
        padding: '5% 6%',
        margin: '3%'
    },
    button: {
        maxHeight: '60px'
    },
    icon:{
    fontSize: 300,
    
    }
})

const GOOGLE_CLIENT_ID = process.env.REACT_APP_GOOGLE_CLIENT_ID

function Login({ isAuthenticated, googleAuth }) {
    const classes = useStyles()
    const history = useHistory()

    if (isAuthenticated) {
        history.push('/notes')
    }

    const callBack = () => {
        history.push('/notes')
        window.location.reload(); // Fix 
    }

    const handleGoogleLogin = (googleData) => {
        console.log(googleData)
        googleAuth(googleData, callBack)
    }

    return (
        <div className={clsx(classes.root, classes.dispCenter)} >
            <Paper className={clsx(classes.dispCenter, classes.paper)} elevation={3} >
                <Icon color='inherit' className={classes.icon} component={BitBangIcon}/>

                <GoogleLogin
                className={classes.button}
                    clientId={GOOGLE_CLIENT_ID}
                    buttonText="Log in with Google"
                    onSuccess={handleGoogleLogin}
                    onFailure={handleGoogleLogin}
                    cookiePolicy={'single_host_origin'}
                />
            </Paper>

        </div>
    )
}

const mapState = state => ({
    isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapState, { googleAuth })(Login)