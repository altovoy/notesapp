import { makeStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { useState, useEffect } from 'react'
import { Grid, Button} from '@material-ui/core'

import {Avatar, AppBar, Toolbar, IconButton, Typography} from '@material-ui/core'


import Note from '../molecules/Note'
import AddNote from '../molecules/AddNote'

import { logout } from '../../utils/auth'
import { getNotes, addNote, editNote, deleteNote } from '../../utils/notes'

const useStyles = makeStyles((theme) => ({
    root: {
        margin: 'auto',
        padding: '80px 3%'
    },
    appBar: {
        flexGrow: 1,
    },
    avatar: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    }
}))

function NotesPage({ user, logout }) {
    const classes = useStyles()
    const [notes, setNotes] = useState(null)

    useEffect(() => {
        getNotes(user._id).then(res => {
            setNotes(res)
        })
    }, [])

    const handleAdd = (data) => {
        const concat = { ...data, author: user._id }
        addNote(concat).then(res => {
            const tmp = notes.concat(res)
            setNotes(tmp)
        })
    }

    const handleEdit = (id, data) => {
        editNote(id, data).then(res => {
            const tmp = notes.map(item => (item._id === id) ? data : item)
            setNotes(tmp)
        })
    }

    const handleDelete = (id) => {
        deleteNote(id).then(res => {
            const tmp = notes.filter(item => item._id !== id)
            setNotes(tmp)
        })
    }

    const handleLogout = e => {
        e.preventDefault()
        logout()
    }

    return (

        <div className={classes.root}>
            <AppBar  position="fixed">
                <Toolbar>
                    <Avatar className={classes.avatar} src={user.picture} />
                    <Typography variant="h6" className={classes.title}>
                        {user.name}</Typography>
                    <Button color="inherit" onClick={handleLogout} >Logout</Button>
                </Toolbar>
            </AppBar>

            <Grid container xs={12} spacing={2}>
                <Grid item spacing={2} xl={2} lg={3} md={4} sm={6} xs={12} >
                    <AddNote onAdd={handleAdd} />
                </Grid>
                {
                    notes && notes.map(note =>
                        <Grid item spacing={2} xl={2} lg={3} md={4} sm={6} xs={12} >
                            <Note data={note} onEdit={handleEdit} onDelete={handleDelete} />
                        </Grid>
                    )
                }

            </Grid>
        </div>
    )

}

export default withRouter(connect(state => ({ user: state.auth.user }), { logout })(NotesPage))