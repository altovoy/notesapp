import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { ThemeProvider } from '@material-ui/core/styles'
import { CssBaseline } from '@material-ui/core'
import theme from './styles/theme'


import NotesPage from './components/pages/NotesPage'
import LoginPage from './components/pages/LoginPage'

// Redux
import { Provider } from "react-redux";
import store from "./store";

import PrivateRoute from './components/routing/PrivateRoute'

function App() {
  return (
    <Router>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <CssBaseline />

          <Switch>

            <PrivateRoute exact path='/notes' component={NotesPage} />
            <Route path='/'><LoginPage /></Route>
          </Switch>

        </ThemeProvider>
      </Provider >
    </Router >
  );
}

export default App;
