import axios from 'axios'

import { SET_CURRENT_USER } from './types'

import * as Cookies from "js-cookie";


const BASE_URI = (process.env.REACT_APP_API_URI || 'http://localhost:4000/api') + '/auth'

export const googleAuth = (googleData, callBack )=> dispatch => {
    axios.post(BASE_URI + '/google', { token: googleData.tokenId })
        .then(res => {
            setSessionCookie(res.data)
            dispatch(setCurrentUser(res.data))
            callBack()
        }).catch(error => {
            console.log(error)
        })
}

export const logout = () => dispatch => {
    axios.delete(BASE_URI + '/logout')
        .then(res => {
            setSessionCookie({})
            dispatch(setCurrentUser({}))
        }).catch((error) => {
            console.log(error)
        })
}

export const setCurrentUser = (user) => ({
    type: SET_CURRENT_USER,
    dispatch: user
})


export const setSessionCookie = (session) => {
    Cookies.remove("session");
    Cookies.set("session", session, { expires: 14 });
};

export const getSessionCookie = () => {
    const sessionCookie = Cookies.get("session");

    if (sessionCookie === undefined) {
        return {};
    } else {
        return JSON.parse(sessionCookie);
    }
};
