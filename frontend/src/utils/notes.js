
import axios from 'axios'

const API_URI = process.env.API_URI || 'http://localhost:4000/api'
const BASE_URI = API_URI+'/notes/'
export const getNotes = async(author=null) => {
    let notes = null
    try{
        const response = await axios.get(BASE_URI, {params:{author}})
        notes = response.data
        return notes
    }catch(error){
        console.log(error)
    }
   
}

export const addNote = async(note) =>{
    try{
        const response = await axios.post(BASE_URI, note)
        return response.data
    }catch(error){
        console.log(error)
    }
}

export const editNote = async(id, note) =>{
    try{
        const response = await axios.put(BASE_URI+`/${id}`, note)
        return response.data
    }catch(error){
        console.log(error)
    }
}


export const deleteNote = async(id) =>{
    try{
        const response = await axios.delete(BASE_URI+`/${id}`)
        return response.data
    }catch(error){
        console.log(error)
    }
}
