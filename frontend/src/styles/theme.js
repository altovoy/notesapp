import {createMuiTheme, responsiveFontSizes} from '@material-ui/core/styles'
import {blue, pink} from '@material-ui/core/colors'

let theme = createMuiTheme(
    {
        palette: { 
            type: 'dark',
            primary: blue,
            secondary: pink
        }
    }
)


theme = responsiveFontSizes(theme)


export default theme