let authCtrl = {}

const User = require('../models/User')
const { OAuth2Client } = require('google-auth-library')
const client = new OAuth2Client(process.env.CLIENT_ID)

authCtrl.googleAuth = async (req, res) => {
    const { token } = req.body
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID
    });
    const { name, email, picture } = ticket.getPayload();

    const user = await User.findOneAndUpdate({ email }, { name, email, picture }, { upsert: true })

    req.session.userId = user.id

    res.status(201)
    res.json(user)

}

authCtrl.logout = async (req, res) => {
    await req.session.destroy()
    res.status(200)
    res.json({
        message: "Logged out successfully"
    })
}

authCtrl.loggedUser = async (req, res) => {
    res.status(200)
    res.json(req.user)
}

module.exports = authCtrl