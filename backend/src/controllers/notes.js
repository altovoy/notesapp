const notesCtrl = {}
const Note = require('../models/Note')

notesCtrl.getNotes = async (req, res) => {
    const { author } = req.query
    const notes = await Note.find(author?{author}:null);
    res.json(notes);
};

notesCtrl.addNote = async (req, res) => {
    const { content, date, author } = req.body;
    const newNote = new Note({
        content,
        author,
        date,
    });
    const not = await newNote.save();
    res.json(not);
};

notesCtrl.getNote = async(req, res) => {
    const {id} = req.params;
    const note = await Note.findById(id)
    res.json(note)
}

notesCtrl.editNote = async (req, res) => {
    const content = req.body
    const contUp = await Note.findByIdAndUpdate(req.params.id, content)
    res.json(contUp)
}

notesCtrl.removeNote = async(req, res) => {
    await Note.findByIdAndRemove(req.params.id)
    res.json('ok')
}

module.exports = notesCtrl