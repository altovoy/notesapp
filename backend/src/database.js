const mongoose = require('mongoose')

const DB_URI = process.env.MONGO_URI || 'mongodb://localhost/notesApp'

mongoose.connect(DB_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})


const connection = mongoose.connection

connection.once('open', () => {
    console.log('Database is connected')
})