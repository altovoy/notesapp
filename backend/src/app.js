const express = require('express')
const cors = require('cors')
const session = require('express-session');

const app = express()

// Settings
app.set('port', process.env.PORT || 4000)

// Middlewares
app.use(cors())
app.use(express.json())

// User Auth middleware

app.use(
    session({
      secret: 'my-secret',
      resave: false,
      saveUninitialized: true
    })
  );
app.use(require('./middlewares/auth'))


// Routes
app.use('/api/notes', require('./routes/notes'))
app.use('/api/auth', require('./routes/auth'))

app.use('/api/', (req, res) => {
    res.send('API OK!')
})


module.exports = app