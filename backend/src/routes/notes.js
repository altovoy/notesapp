const { Router } = require('express');
const router = Router();

const { getNotes, addNote, editNote, getNote, removeNote} = require('../controllers/notes');

router.route('/')
    .get(getNotes)
    .post(addNote)
    
router.route('/:id')
    .get(getNote)
    .delete(removeNote)
    .put(editNote)


module.exports = router;