const express = require('express')
const router = express.Router()

const { googleAuth, logout, loggedUser} = require('../controllers/auth')

router.route('/google')
    .post(googleAuth)

router.route('/me')
    .delete(loggedUser)

router.route('/logout')
    .delete(logout)

module.exports = router