const { Schema, model } = require('mongoose');

const noteSchema = new Schema(
    {
        content: { type: String},
        author: { type: String },
        visible: Boolean,
        date: Date
    }, {
        timestamps: true
});

module.exports = model('Note', noteSchema);